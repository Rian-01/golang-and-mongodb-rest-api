// Call package
package controllers

import (
	"Mongo-golang/models"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// create struct
type UserControllers struct {
	session *mgo.Session
}

// create func
func NewUserControllers(s *mgo.Session) *UserControllers {
	// memngembalikan value
	return &UserControllers{s}
}

// create func
func (uc UserControllers) GetUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Dec variable
	id := p.ByName("id")

	// condition
	if !bson.IsObjectIdHex(id) {
		w.WriteHeader(http.StatusNotFound)
	}

	// Dec variabel
	oid := bson.ObjectIdHex(id)
	u := models.User{}

	// condition
	if err := uc.session.DB("mongo-golang").C("users").FindId(oid).One(&u); err != nil {
		w.WriteHeader(404)
		return
	}

	uj, err := json.Marshal(u)
	// condition
	if err != nil {
		fmt.Println(err)
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj)
}

// Create func
func (uc UserControllers) CreateUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	u := models.User{}

	json.NewDecoder(r.Body).Decode(&u)

	u.ID = bson.NewObjectId()

	uc.session.DB("Mongo-golang").C("users").Insert(u)

	uj, err := json.Marshal(u)
	// condition
	if err != nil {
		fmt.Println("Error")
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	fmt.Fprintf(w, "%s\n", uj)
}

// Create func
func (uc UserControllers) DeleteUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	id := p.ByName("id")

	if !bson.IsObjectIdHex(id) {
		w.WriteHeader(404)
		return
	}

	oid := bson.ObjectIdHex(id)
	if err := uc.session.DB("mongo-golang").C("users").RemoveId(oid); err != nil {
		w.WriteHeader(404)
	}
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "Delete user", oid, "\n")
}
