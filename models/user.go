// Call package
package models

import "gopkg.in/mgo.v2/bson"

// Create struct user
type User struct {
	// Json body
	ID     bson.ObjectId `json:"id" bson:"_id"`
	Name   string        `json:"name" bson:"name"`
	Gender string        `json:"gender" bson:"gender"`
	Age    int           `json:"age" bson:"age"`
}
