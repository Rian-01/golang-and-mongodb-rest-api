// Memanggil package
package main

import (
	"Mongo-golang/controllers"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
)

// Run func main
func main() {
	r := httprouter.New()
	uc := controllers.NewUserControllers(getSession())

	// Endpoint
	r.GET("/user", uc.GetUser)
	r.POST("/regis", uc.CreateUser)
	r.DELETE("/delete/:id", uc.DeleteUser)
	http.ListenAndServe("localhost:8080", r)
}

// Run func
func getSession() *mgo.Session {

	s, err := mgo.Dial("mongodb://localhost:27017")
	if err != nil {
		panic(err)
		// fmt.Println("error")
	}
	return s
}
